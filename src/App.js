import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import LeagueService from './LeagueService';

import './App.css';
import PlayersList from './components/PlayersList';
import TeamsList from './components/TeamsList';
import { LOGO_URL, PLAYER_IMG_URL } from './common/constants';
import playerPlaceholder from './person-placeholder.png';
import first from './first.png';
import second from './second.png';
import third from './third.png';

class App extends Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.leagueService = new LeagueService();
    this.state = {
      teams: [],
      players: [],
      isLoading: true,
      selectedTeams: [],
      selectedPlayer: '',
      fplTeams: [],
      fplPlayers: []
    }

    this.handlePlayerChange = this.handlePlayerChange.bind(this);
    this.onTeamToggle = this.onTeamToggle.bind(this);
    this.getLeagueData = this.getLeagueData.bind(this);
    this.getFplData = this.getFplData.bind(this);
  }
  getLeagueData() {
    const leagueData = this.leagueService.getLeagueData();
    let players = [];
    leagueData.then(response => {

      let teams = response;
      for (let tindex = 0; tindex < teams.length; tindex++) {
        const team = teams[tindex];
        team.checked = false;
        for (let pindex = 0; pindex < team.topPlayers.length; pindex++) {
          const player = team.topPlayers[pindex];
          players.push(player);
        }
      }
      this.setState({ isLoading: false, teams, players });
    })
  }
  getFplData() {
    const fplData = this.leagueService.getFplData();
    fplData.then(response => {
      let { teams, elements } = response;
      this.setState({ fplTeams: teams, fplPlayers: elements })
    })
      .catch(error => {
        console.log('err', error)
      })
  }
  componentDidMount() {
    this.getLeagueData();
    this.getFplData();
  }
  onTeamToggle(event) {
    let index = event.target.id;
    let selectedTeams;
    // Clone the array
    let teamsCopy = this.state.teams.slice(0);
    if (this.state.selectedTeams.length === 3 && !teamsCopy[index].checked) {
      let myRef = ReactDOM.findDOMNode(this).getElementsByClassName("teams-checkbox-" + index)[0];
      myRef.checked = false;
      return;
    }
    teamsCopy[index].checked = !teamsCopy[index].checked;
    let teamName = teamsCopy[index].teamName;
    let fplTeamsCopy = this.state.fplTeams.slice(0);
    teamName = teamName.replace('Manchester', 'Man');
    teamName = teamName.replace('United', 'Utd');
    teamName = teamName.replace('Tottenham', 'Spurs');
    let fplTeam = fplTeamsCopy.filter(x => x.name === teamName)[0];
    if (fplTeam) {
      teamsCopy[index].code = fplTeam.code;
    }
    selectedTeams = teamsCopy.filter(selected => selected.checked);

    this.setState({
      teams: teamsCopy,
      selectedTeams
    })
  }
  handlePlayerChange(event) {
    let getPlayerLastName = event.target.value;
    getPlayerLastName = getPlayerLastName.split(' ').pop();
    let fplPlayersCopy = this.state.fplPlayers.slice(0);
    let foundPlayer = fplPlayersCopy.filter(x => x.second_name === getPlayerLastName)[0];
    let selectedPlayer = {};
    selectedPlayer.playerName = event.target.value;
    if (foundPlayer) {
      selectedPlayer.code = foundPlayer.code;
    }
    this.setState({ selectedPlayer });
  }
  render() {
    const { isLoading } = this.state;
    if (isLoading) {
      return <div>
        <div><p>Loading ...</p></div>
      </div>;
    }
    return (
      <div className="App">

        <div className="section group">
          <div className="col span_1_of_3">
            <h2 className="list-header teams-list-header">Predict top 3 teams:</h2>
            <TeamsList teams={this.state.teams} action={this.onTeamToggle} inputRef={el => this.inputElement = el} />
          </div>
          <div className="col span_1_of_3">
            <h2 className="list-header top-scorer-header">Predict top scorer:</h2>
            <PlayersList players={this.state.players} selectedPlayer={this.state.selectedPlayer} action={this.handlePlayerChange} />
          </div>
          <div className="col span_1_of_3">
            <h2 className="list-header summary-header">Summary:</h2>
            <div className="summary-card">
              <div className="summary-card-header">
                <p>You selected: {this.state.selectedPlayer.playerName}</p>
              </div>
              <div>
                <div className="summary-card-body-img">
                  {this.state.selectedPlayer.code ?
                    <img className="player-image" src={`${PLAYER_IMG_URL}p${this.state.selectedPlayer.code}.png`} alt="" /> :
                    <img className="player-image" src={playerPlaceholder} alt="" />
                  }
                </div>
              </div>
              <div>
                <div className="summary-card-places">
                  <div className="summary-places-holder">
                    <img className="team-logo" src={first} alt="" />
                  </div>
                  <div className="summary-places-holder">
                    <img className="team-logo" src={second} alt="" />
                  </div>
                  <div className="summary-places-holder">
                    <img className="team-logo" src={third} alt="" />
                  </div>
                </div>
                <div className="summary-card-logo">

                  {this.state.selectedTeams.map((item, index) =>
                    <div className="summary-places-holder" key={index}>
                      <img className="team-logo" src={`${LOGO_URL}t${item.code}.svg`} alt="" />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default App;
