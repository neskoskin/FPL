export const CORS_URL = 'https://cors-anywhere.herokuapp.com/';
export const BASE_URL = 'https://s3-eu-west-1.amazonaws.com/forza-home-assignment/refinery-frontend/league-data.json';
export const FPL_URL = 'https://fantasy.premierleague.com/drf/bootstrap-static';
export const LOGO_URL = 'https://platform-static-files.s3.amazonaws.com/premierleague/badges/';
export const PLAYER_IMG_URL = 'https://platform-static-files.s3.amazonaws.com/premierleague/photos/players/110x140/';