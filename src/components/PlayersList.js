import React, { Component } from "react";
class PlayersList extends Component {
    render() {
        const { selectedPlayer, players, action } = this.props;
        return (
            <div>
                <div className="select-wrapper">
                    <select className="select-player" value={selectedPlayer.playerName} onChange={action}>
                        <option>None</option>
                        {players.map((player) =>
                            <option key={player.playerId} value={player.playerName}>{player.playerName}</option>
                        )}
                    </select>
                </div>
            </div>
        )
    }
}
export default PlayersList;