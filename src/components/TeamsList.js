import React, { Component } from "react";
class TeamsList extends Component {
    render() {
        const { teams, action } = this.props;
        return (
            <div>
                <ul>
                    {teams.map((item, i) =>
                        <li key={item.teamId}>
                            {item.teamName}
                            <input
                                type="checkbox"
                                className={"teams-checkbox-" + i}
                                id={i}
                                onChange={action}
                            />
                            <label htmlFor={i}>
                                <i className="fa fa-check"></i>
                            </label>
                        </li>
                    )}
                </ul>
            </div>
        )
    }
}
export default TeamsList;