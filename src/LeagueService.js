import { BASE_URL, CORS_URL, FPL_URL } from './common/constants';
import axios from 'axios';

export default class LeagueService {
    constructor() {
        this.url = BASE_URL;
        this.corsUrl = CORS_URL;
        this.fplUrl = FPL_URL;
        this.getLeagueData = this.getLeagueData.bind(this);
    }
    getLeagueData() {
        return axios.get(`${this.corsUrl}${this.url}`)
            .then(response => {
                return response.data.leagues[0].teams
            })
            .catch(error => console.log(error));
    }
    getFplData() {
        return axios.get(`${this.corsUrl}${this.fplUrl}`)
            .then(response => {
                return response.data
            })
            .catch(error => console.log(error))
    }
}